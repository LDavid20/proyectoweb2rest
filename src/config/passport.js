const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy
const User = require('../models/User');

const jwt = require('jsonwebtoken');
const config = require('./config');

/** Verifica la autentificacion del usuario p*/
passport.use(new LocalStrategy({
  usernameField: 'username',
  passwordField: 'password',
}, async (username, password, done,res) => {
  const user = await User.findOne({ username: username });
  if (!user) {
    return done(null, false, { message: 'Not User found.' });
  } else {
    // Match Password's User
    const match = await user.matchPassword(password);
    if (match) {
      const token = jwt.sign({ id: user._id }, config.secret, {
        expiresIn: 60 * 60 * 24
      });
      // console.log("HOOOOOOOOOOOOOOOOOLLLLLLLLLLLLLLLAAAAAAAAAAAA")
      // user.token = token;
      // console.log(user.token)
      console.log("TOCKEN")
      console.log(token)
      // const decoded = await jwt.verify(token, config.secret);
      // userID = decoded.id;
      // res.status(200).json({ auth: true, token });
      return done(null, user);
    } else {
      return done(null, false, { message: 'Incorrect Password.' });
    }
  }
}));

/** Guarda el usuario*/
passport.serializeUser((user, done) => {
  done(null, user.id);
});

/**Comprobar su el id del usuario existe */
passport.deserializeUser((id, done) => {
  User.findById(id, (err, user) => {
    done(err, user);
  }).lean();
});