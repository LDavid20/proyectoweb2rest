const clientCtrl = {};
const Client = require('../models/Client');

/*Metodo que carga el formulario de crear client*
 * 
 * @param {*} req 
 * @param {*} res 
 */
clientCtrl.renderClientForm = (req, res) => {
    res.render('clients/new-client')
}

/*Metodo que crear un nuevo client*
 * 
 * @param {*} req 
 * @param {*} res 
 */
clientCtrl.createNewClient = async (req, res) => {
    const { name, idJuridica, web, address, phone, sector } = req.body;

    const newClient = new Client({ name, idJuridica, web, address, phone, sector });
    await newClient.save();
    req.flash('success_msg', 'Client Added Successfully')
    res.redirect('/clients');
}

/*Metodo que carga todos los clients*
 * 
 * @param {*} req 
 * @param {*} res 
 */
clientCtrl.renderClient = async (req, res) => {
    const clients = await Client.find();
    res.render('clients/all-clients', { clients });
}

/*Metodo que muestra el client*
 * 
 * @param {*} req 
 * @param {*} res 
 */
clientCtrl.renderViewForm = async (req, res) => {
    const client = await Client.findById(req.params.id).lean();
    res.render("clients/view-client", { client });
};

/*Metodo que carga el formulario de editar client*
 * 
 * @param {*} req 
 * @param {*} res 
 */
clientCtrl.renderEditForm = async (req, res) => {
    const client = await Client.findById(req.params.id).lean();
    res.render('clients/edit-client', { client });
};

/*Metodo que actualiza el client*
 * 
 * @param {*} req 
 * @param {*} res 
 */
clientCtrl.updateClient = async (req, res) => {
    const { name, idJuridica, web, address, phone, sector } = req.body;
    
    await Client.findByIdAndUpdate(req.params.id, { name, idJuridica, web, address, phone, sector })
    req.flash('success_msg', 'Client Update Successfully')
    res.redirect('/clients')
}

/*Metodo que elimina el client*
 * 
 * @param {*} req 
 * @param {*} res 
 */
clientCtrl.deleteClient = async (req, res) => {
    await Client.findByIdAndDelete(req.params.id)
    req.flash('success_msg', 'Client Added Successfully')
    res.redirect('/clients');
}

module.exports = clientCtrl;