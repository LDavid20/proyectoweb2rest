const contactCtrl = {};

const Contact = require("../models/Contact");

/*Metodo que carga el formulario de crear contact*
 * 
 * @param {*} req 
 * @param {*} res 
 */
contactCtrl.renderContactForm = (req, res) => {
    const idClient = req.params.id;

    res.render('contacts/new-contact', { idClient });
};

/*Metodo que carga todos los contactos*
 * 
 * @param {*} req 
 * @param {*} res 
 */
contactCtrl.renderAllContact = async (req, res) => {
    const idClient = req.params.id;

    const contacts = await Contact.find({ client: idClient });
    res.render("contacts/all-contacts", { contacts, idClient });

}

/*Metodo que crear el contacto*
 * 
 * @param {*} req 
 * @param {*} res 
 */
contactCtrl.createNewContact = async (req, res) => {
    const { client, name, lastname, email, phone, job } = req.body;

    const errors = [];
    if (!name) {
        errors.push({ text: "Please Write a Name" });
    }
    if (!lastname) {
        errors.push({ text: "Please Write a Last Name" });
    }
    if (!email) {
        errors.push({ text: "Please Write a Email" });
    }
    if (!phone) {
        errors.push({ text: "Please Write a Phone" });
    }
    if (!job) {
        errors.push({ text: "Please Write a Job" });
    }
    if (errors.length > 0) {
        res.render("contacts/new-contact", {
            client,
            name,
            lastname,
            email,
            phone,
            job
        });
    } else {
        const newContact = new Contact({ client, name, lastname, email, phone, job });
        await newContact.save();
        req.flash("success_msg", "Contact Added Successfully");
        res.redirect("/contacts/" + client);
    }
};

/*Metodo que carga todo los contacts del client*
 * 
 * @param {*} req 
 * @param {*} res 
 */
contactCtrl.renderContact = async (req, res) => {
    const idClient = req.params.id;

    const contacts = await Contact.find({ client: idClient });
    res.render("contacts/all-contacts", { contacts, idClient });
};


/*Metodo que muestra el contact*
 * 
 * @param {*} req 
 * @param {*} res 
 */
contactCtrl.renderViewForm = async (req, res) => {
    const contact = await Contact.findById(req.params.id).lean();
    res.render("contacts/view-contact", { contact });
};

/*Metodo que carga el formulario de editar contact*
 * 
 * @param {*} req 
 * @param {*} res 
 */
contactCtrl.renderEditForm = async (req, res) => {
    const contact = await Contact.findById(req.params.id).lean();
    res.render("contacts/edit-contact", { contact });
};

/*Metodo que actualiza el contact*
 * 
 * @param {*} req 
 * @param {*} res 
 */
contactCtrl.updateContact = async (req, res) => {
    const { client, name, lastname, email, phone, job } = req.body;

    await Contact.findByIdAndUpdate(req.params.id, { client, name, lastname, email, phone, job });
    req.flash("success_msg", "Contact Updated Successfully");
    res.redirect("/contacts/" + client);
};

/*Metodo que elimina el contact*
 * 
 * @param {*} req 
 * @param {*} res 
 */
contactCtrl.deleteContact = async (req, res) => {
    const { idClient } = req.body;

    await Contact.findByIdAndDelete(req.params.id);
    req.flash("success_msg", "Contacts Deleted Successfully");
    res.redirect("/contacts/" + idClient);
};


module.exports = contactCtrl;