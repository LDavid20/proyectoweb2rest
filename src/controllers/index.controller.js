const indexCtrl = {};

/*Metodo que carga el index*
 * 
 * @param {*} req 
 * @param {*} res 
 */
indexCtrl.renderIndex = (req, res) => {
    res.render('index')
}

/*Metodo que carga el about*
 * 
 * @param {*} req 
 * @param {*} res 
 */
indexCtrl.renderAbout = (req, res) => {
    res.render('about')
}

module.exports = indexCtrl;