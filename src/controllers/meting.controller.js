const metingCtrl = {};
const Meting = require("../models/Meting");
const User = require("../models/User");
const Client = require("../models/Client");

/*Metodo que carga el formulario de Meting*
 * 
 * @param {*} req 
 * @param {*} res 
 */
metingCtrl.renderMetingForm = async (req, res) => {
    const users = await User.find().lean();
    const clients = await Client.find().lean();
    res.render('metings/new-meting', { users, clients });

};

/*Metodo que crear el meting*
 * 
 * @param {*} req 
 * @param {*} res 
 */
metingCtrl.createNewMeting = async (req, res) => {
    const { title, date, user, isVirtual, client } = req.body;

    const errors = [];
    console.log(isVirtual.checked);
    if (!title) {
        errors.push({ text: "Please Write a Title." });
    }
    if (!date) {
        errors.push({ text: "Please Write a Date" });
    }

    if (!user) {
        errors.push({ text: "Please Write a User" });
    }
    if (!client) {
        errors.push({ text: "Please Write a Client" });
    }
    if (errors.length > 0) {
        res.render("metings/new-meting", {
            errors,
            title,
            date,
            user,
            client
        });
    } else {
        const newMeting = new Meting({ title, date, user, isVirtual, client });

        await newMeting.save();
        req.flash("success_msg", "Meting Added Successfully");
        res.redirect("/metings");
    }
};

/*Metodo que carga todos las metings*
 * 
 * @param {*} req 
 * @param {*} res 
 */
metingCtrl.renderMeting = async (req, res) => {
    const metings = await Meting.find();

    res.render("metings/all-metings", { metings });

};

/*Metodo que muestra el meting*
 * 
 * @param {*} req 
 * @param {*} res 
 */
metingCtrl.renderViewForm = async (req, res) => {
    const users = await User.find().lean();
    const clients = await Client.find().lean();

    const meting = await Meting.findById(req.params.id).lean();
    res.render("metings/view-meting", { meting, users, clients });
};

/*Metodo que carga el formulario de editar meting*
 * 
 * @param {*} req 
 * @param {*} res 
 */
metingCtrl.renderEditForm = async (req, res) => {

    const users = await User.find().lean();
    const clients = await Client.find().lean();

    const meting = await Meting.findById(req.params.id).lean();
    res.render("metings/edit-meting", { meting, users, clients });

};

/*Metodo que actualiza el meting*
 * 
 * @param {*} req 
 * @param {*} res 
 */
metingCtrl.updateMeting = async (req, res) => {
    const { title, date, user, isVirtual, client } = req.body;

    await Meting.findByIdAndUpdate(req.params.id, { title, date, user, isVirtual, client });
    req.flash("success_msg", "Meting Updated Successfully");
    res.redirect("/metings");
};

/*Metodo que elimina el meting*
 * 
 * @param {*} req 
 * @param {*} res 
 */
metingCtrl.deleteMeting = async (req, res) => {
    await Meting.findByIdAndDelete(req.params.id);
    req.flash("success_msg", "Meting Deleted Successfully");
    res.redirect("/metings");
};


module.exports = metingCtrl;