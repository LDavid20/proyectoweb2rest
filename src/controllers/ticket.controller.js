const ticketCtrl = {};
const Ticket = require("../models/Ticket");
const Client = require("../models/Client");

/*Metodo que carga el formulario de crear ticket*
 * 
 * @param {*} req 
 * @param {*} res 
 */
ticketCtrl.renderTicketForm = async (req, res) => {
    const idClient = req.params.id;
    res.render('tickets/new-ticket', { idClient });
};

/*Metodo que crear un nuevo ticket al client*
 * 
 * @param {*} req 
 * @param {*} res 
 */
ticketCtrl.createNewTicket = async (req, res) => {
    const { title, detail, person, client, state } = req.body;

    const errors = [];
    if (!title) {
        errors.push({ text: "Please Write a Title." });
    }
    if (!detail) {
        errors.push({ text: "Please Write a Detail" });
    }

    if (!person) {
        errors.push({ text: "Please Write a Person" });
    }
    if (!client) {
        errors.push({ text: "Please Write a Client" });
    }
    if (!state) {
        errors.push({ text: "Please Write a State" });
    }

    if (errors.length > 0) {
        res.render("tickets/new-ticket", {
            errors,
            title,
            detail,
            person,
            client,
            state
        });
    } else {
        const newTicket = new Ticket({ title, detail, person, client, state });

        await newTicket.save();
        req.flash("success_msg", "Ticket Added Successfully");
        res.redirect("/tickets/" + client);
    }
};

/*Metodo que carga todos los tickets*
 * 
 * @param {*} req 
 * @param {*} res 
 */
ticketCtrl.renderTicket = async (req, res) => {
    const idClient = req.params.id;
    const tickets = await Ticket.find({ client: idClient });
    res.render("tickets/all-tickets", { tickets, idClient });

};

/*Metodo que cargar el view del formulario*
 * 
 * @param {*} req 
 * @param {*} res 
 */
ticketCtrl.renderViewForm = async (req, res) => {
    const clients = await Client.find().lean();
    const ticket = await Ticket.findById(req.params.id).lean();
    res.render("tickets/view-ticket", { ticket, clients });
};

/*Metodo que carga el formularios de editar el ticket*
 * 
 * @param {*} req 
 * @param {*} res 
 */
ticketCtrl.renderEditForm = async (req, res) => {
    const clients = await Client.find().lean();
    const ticket = await Ticket.findById(req.params.id).lean();
    res.render("tickets/edit-ticket", { ticket, clients });
};

/*Metodo se encarga de actualizar el ticket*
 * 
 * @param {*} req 
 * @param {*} res 
 */
ticketCtrl.updateTicket = async (req, res) => {
    const { title, detail, person, client, state } = req.body;
    
    await Ticket.findByIdAndUpdate(req.params.id, { title, detail, person, client, state });
    req.flash("success_msg", "Ticket Updated Successfully");
    res.redirect("/tickets/" + client);
};

/*Metodo que elimina el ticket por el id*
 * 
 * @param {*} req 
 * @param {*} res 
 */
ticketCtrl.deleteTicket = async (req, res) => {
    const { idClient } = req.body;

    await Ticket.findByIdAndDelete(req.params.id);
    req.flash("success_msg", "Ticket Deleted Successfully");
    res.redirect("/tickets/" + idClient);
};


module.exports = ticketCtrl;