const userCtrl = {};
const passport = require('passport');
const User = require('../models/User');


const jwt = require('jsonwebtoken');
const config = require('../config/config');
/*Metodo que carga la bienvenida el usuario*
 * 
 * @param {*} req 
 * @param {*} res 
 */
userCtrl.renderStart = (req, res) => {
    res.render('users/start');
};

/*Metodo que carga el formulario de crear user*
 * 
 * @param {*} req 
 * @param {*} res 
 */
userCtrl.renderSignUpForm = (req, res) => {
    res.render('users/signup');
}

/*Metodo que crea el user*
 * 
 * @param {*} req 
 * @param {*} res 
 */
userCtrl.signUp = async (req, res) => {
    let errors = [];
    const { name, lastname, username, password, confirm_password } = req.body;
    if (password != confirm_password) {
        errors.push({ text: "Passwords do not match" });
    }
    if (password.length < 4) {
        errors.push({ text: "Passwords must be at least 4 characters." });
    }
    if (errors.length > 0) {
        res.render("users/signup", {
            errors,
            name,
            lastname,
            username,
            password,
            confirm_password
        });
    } else {
        const Username = await User.findOne({ username: username });
        if (Username) {
            req.flash("error_msg", "The User Name is already in use.");
            res.redirect("/users/signup");
        } else {
            rol = 0;
            const newUser = new User({ name, lastname, username, password, rol });
            newUser.password = await newUser.encryptPassword(password);
            await newUser.save();
            // Create a Token
            const token = jwt.sign({ id: newUser.id }, config.secret, {
                expiresIn: 60 * 60 * 24 // expires in 24 hours
            });
            console.log(token);
            // res.json({ auth: true, token });
            console.log(newUser);
            req.flash("success_msg", "You are registered.");
            res.redirect("/users");
        }
    }
}

/*Metodo que carga el formulario de signin*
 * 
 * @param {*} req 
 * @param {*} res 
 */
userCtrl.renderSignInForm = (req, res) => {
    res.render('users/signin');
}

/*Metodo que autentica el user*
 * 
 */
userCtrl.signIn = passport.authenticate("local", {
    successRedirect: "/start",
    failureRedirect: "/users/signin",
    failureFlash: true
});

/*Metodo que cierra la seccion del user*
 * 
 * @param {*} req 
 * @param {*} res 
 */
userCtrl.logout = (req, res) => {
    req.logout();
    req.flash("success_msg", "You are logged out now.");
    res.redirect("/users/signin");
}

/*Metodo que carga todos los user*
 * 
 * @param {*} req 
 * @param {*} res 
 */
userCtrl.renderUser = async (req, res) => {
    const users = await User.find();
    res.render("users/all-users", { users });
};

/*Metodo que muestra el user*
 * 
 * @param {*} req 
 * @param {*} res 
 */
userCtrl.renderViewForm = async (req, res) => {
    const users = await User.findById(req.params.id).lean();
    res.render("users/view-user", { users });
};

/*Metodo que carga el formulario de editar user*
 * 
 * @param {*} req 
 * @param {*} res 
 */
userCtrl.renderEditForm = async (req, res) => {
    const users = await User.findById(req.params.id).lean();
    res.render("users/edit-user", { users });
};

/*Metodo que actualiza el usuario*
 * 
 * @param {*} req 
 * @param {*} res 
 */
userCtrl.updateUser = async (req, res) => {
    let errors = [];
    let { name, lastname, username, password, confirm_password } = req.body;
    if (password != confirm_password) {
        errors.push({ text: "Passwords do not match." });
    }
    if (password.length < 4) {
        errors.push({ text: "Passwords must be at least 4 characters." });
    }
    if (errors.length > 0) {
        req.flash("error_msg", "Invalid Password");
        res.redirect("/users");
        // res.render("users/edit/"+req.params.id,  {
        //     errors,
        //     name,
        //     lastname,
        //     username,
        //     password,
        //     confirm_password
        // });
    } else {

        const newUser = new User({ name, lastname, username, password });
        newUser.password = await newUser.encryptPassword(password);
        password = newUser.password

        await User.findByIdAndUpdate(req.params.id, { name, lastname, username, password });
        req.flash("success_msg", "User Updated Successfully");
        res.redirect("/users");
    }
};
/*Metodo que elimina el usuario*
 * 
 * @param {*} req 
 * @param {*} res 
 */
userCtrl.deleteUser = async (req, res) => {
    await User.findByIdAndDelete(req.params.id);
    req.flash("success_msg", "User Deleted Successfully");
    res.redirect("/users");
};

module.exports = userCtrl;