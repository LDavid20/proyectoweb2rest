const jwt = require('jsonwebtoken');
const config = require('../config/config');
const User = require('../models/User');

async function verifyToken(req, res, next) {
    const user = await User.findOne(req.params.username);
    const token = req.headers['x-access-token'];
    console.log(user.token)
    if (!req.user.token) {
        return res.status(401).send({ auth: false, message: 'No token provided' });
    }
    // Decode the Tokenreq.userId = decoded.id;
    const decoded = await jwt.verify(token, config.secret);
    req.userId = decoded.id;
    next();
}

module.exports = verifyToken;   