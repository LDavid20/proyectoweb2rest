const helpers = {};

/*Metodo que autentifica la pantalla*
 * 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
helpers.isAuthenticated = (req, res, next) => {
  if (req.isAuthenticated()) {
    return next();
  }
  req.flash('error_msg', 'Not Authorized.');
  res.redirect('/users/signin');
};

module.exports = helpers;
