const { Schema, model } = require('mongoose');

const client = new Schema({

    name: { type: String, required: true },
    idJuridica: { type: String, required: true },
    web: { type: String, required: true },
    address: { type: String, required: true },
    phone: { type: String, required: true },
    sector: { type: String, required: true }

}, {
    timestamps: true
});

module.exports = model('clients', client);