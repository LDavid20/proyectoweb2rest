const { Schema, model } = require('mongoose');

const contact = new Schema({
    client: { type: String, required: true },
    name: { type: String, required: true },
    lastname: { type: String, required: true },
    email: { type: String, required: true },
    phone: { type: String, required: true },
    job: { type: String, required: true }

});

module.exports = model('contacts', contact);



