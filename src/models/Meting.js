const { Schema, model } = require('mongoose');

const meting = new Schema({
    title: { type: String, required: true },
    date: { type: String, required: true },
    user: { type: String, required: true },
    isVirtual: { type: String, required: true },
    client: { type: String }
});

module.exports = model('metings', meting);