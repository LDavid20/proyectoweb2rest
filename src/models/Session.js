const { Schema, model } = require('mongoose');

const session = new Schema({
    token: { type: String },
    user: { type: String },
    expire: { type: Date }
});

module.exports = model('sessions', session);