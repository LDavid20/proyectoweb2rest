const { Schema, model } = require('mongoose');

const ticket = new Schema({
    title: { type: String, required: true },
    detail: { type: String, required: true },
    person: { type: String, required: true },
    client: { type: String, required: true },
    state: { type: String, required: true }
});

module.exports = model('tickets', ticket);