const { Schema, model } = require('mongoose');
const bcrypt = require('bcryptjs');

const user = new Schema({

    name: { type: String, required: true },
    lastname: { type: String, required: true },
    username: { type: String, required: true },
    password: { type: String, required: true },
    rol: { type: Boolean, required: true }

}, {
    timestamps: true
});

user.methods.encryptPassword = async password => {
    const salt = await bcrypt.genSalt(10);
    return await bcrypt.hash(password, salt);
  };


user.methods.matchPassword = async function (password) {
    return await bcrypt.compare(password, this.password)
}


module.exports = model('users', user);