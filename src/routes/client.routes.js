const { Router } = require('express')
const router = Router();

const {
    renderClientForm,
    createNewClient,
    renderClient,
    renderViewForm,
    renderEditForm,
    updateClient,
    deleteClient
} = require('../controllers/client.controller')

const { isAuthenticated } = require('../helpers/auth')

//New Client
router.get('/clients/add', isAuthenticated, renderClientForm);
router.post('/clients/new-client', isAuthenticated, createNewClient);

//Get All Client
router.get('/clients', isAuthenticated, renderClient);
router.get("/clients/view/:id", isAuthenticated, renderViewForm);

//Edit Client
router.get('/clients/edit/:id', isAuthenticated, renderEditForm);
router.put('/clients/edit/:id', isAuthenticated, updateClient);

//Delete Client
router.delete('/clients/delete/:id', isAuthenticated, deleteClient);



module.exports = router;