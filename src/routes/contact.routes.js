const express = require("express");
const router = express.Router();

const {
    renderContactForm,
    createNewContact,
    updateContact,
    renderContact,
    renderViewForm,
    renderEditForm,
    deleteContact,

} = require("../controllers/contact.controller");

const { isAuthenticated } = require("../helpers/auth");

// New Contact
router.get("/contacts/add/:id", isAuthenticated, renderContactForm);
router.post("/contacts/new-contact", isAuthenticated, createNewContact);

// Get All Clients
router.get("/contacts/:id", isAuthenticated, renderContact);
router.get("/contacts/view/:id", isAuthenticated, renderViewForm);

// Edit Clients
router.get("/contacts/edit/:id", isAuthenticated, renderEditForm);
router.put("/contacts/edit-contact/:id", isAuthenticated, updateContact);

// Delete Client
router.delete("/contacts/delete/:id", isAuthenticated, deleteContact);

module.exports = router;
