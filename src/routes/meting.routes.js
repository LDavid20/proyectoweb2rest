const express = require("express");
const router = express.Router();

const {
    renderMetingForm,
    createNewMeting,
    updateMeting,
    renderMeting,
    renderViewForm,
    renderEditForm,
    deleteMeting,
} = require("../controllers/meting.controller");

const { isAuthenticated } = require("../helpers/auth");

// New Meting
router.get("/metings/add", isAuthenticated, renderMetingForm);
router.post("/metings/new-meting", isAuthenticated, createNewMeting);

// Get All metings
router.get("/metings", isAuthenticated, renderMeting);
router.get("/metings/view/:id", isAuthenticated, renderViewForm);


// Edit metings
router.get("/metings/edit/:id", isAuthenticated, renderEditForm);
router.put("/metings/edit-meting/:id", isAuthenticated, updateMeting);

// Delete Client
router.delete("/metings/delete/:id", isAuthenticated, deleteMeting);

module.exports = router;
