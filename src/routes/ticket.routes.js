const express = require("express");
const router = express.Router();

const {
    renderTicketForm,
    createNewTicket,
    updateTicket,
    renderTicket,
    renderViewForm,
    renderEditForm,
    deleteTicket,
} = require("../controllers/ticket.controller");

const { isAuthenticated } = require("../helpers/auth");

// New Ticket
router.get("/tickets/add/:id", isAuthenticated, renderTicketForm);
router.post("/tickets/new-ticket", isAuthenticated, createNewTicket);

// Get All metings
router.get("/tickets/:id", isAuthenticated, renderTicket);
router.get("/tickets/view/:id", isAuthenticated, renderViewForm);

// Edit metings
router.get("/tickets/edit/:id", isAuthenticated, renderEditForm);
router.put("/tickets/edit-ticket/:id", isAuthenticated, updateTicket);

// Delete Client
router.delete("/tickets/delete/:id", isAuthenticated, deleteTicket);

module.exports = router;
