const { Router } = require('express')
const router = Router();
const verifyToken = require('../controllers/verifyToken.js');

const {
    renderSignInForm,
    renderSignUpForm,
    signIn,
    signUp,
    logout,
    renderUser,
    renderViewForm,
    renderEditForm,
    updateUser,
    deleteUser,
    renderStart
} = require('../controllers/user.controller.js')

const { isAuthenticated } = require("../helpers/auth");

// New User
router.get('/users/signup', isAuthenticated, renderSignUpForm);
router.post('/users/signup', isAuthenticated, signUp);

// Inicial User
router.get('/users/signin', renderSignInForm);
router.post('/users/signin', signIn);

// Logout
router.get('/users/logout', logout);

// View User
router.get("/start", isAuthenticated, renderStart);
router.get("/users", isAuthenticated, renderUser);
router.get("/users/view/:id", isAuthenticated, renderViewForm);

// Edit Clients
router.get("/users/edit/:id", isAuthenticated, renderEditForm);
router.put("/users/edit-user/:id", isAuthenticated, updateUser);

// Delete Client
router.delete("/users/delete/:id", isAuthenticated, deleteUser);

module.exports = router; 