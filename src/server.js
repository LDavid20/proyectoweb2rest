const express = require('express');
const exphbs = require('express-handlebars');
const path = require('path');
const morgan = require('morgan');
const methodOverride = require('method-override');
const flash = require('connect-flash');
const session = require('express-session');
const passport = require('passport');
// const session = require("./models/Session");
// const connectMongo = require('connect-mongo');
// const mongoose = require('mongoose');


//Initializations
const app = express();
require('./config/passport');

//Settings
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.engine('.hbs', exphbs({
    defaultLayout: 'main',
    layoutsDir: path.join(app.get('views'), 'layouts'),
    partialsDir: path.join(app.get('views'), 'partials'),
    extname: '.hbs'
}));
app.set('view engine', '.hbs')

//Middlewares
app.use(morgan('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(methodOverride('_method'));
app.use(session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true,
    // store: new MongoStore({mongooseConnection: mongoose.connection})
}));
app.use(passport.initialize())
app.use(passport.session())
app.use(flash());

//Global Variable
app.use((req, res, next) => {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    res.locals.user = req.user || null;
    next();
});


//Routes
app.use(require('./routes/index.routes'));
app.use(require('./routes/user.routes'));
app.use(require('./routes/client.routes'));
app.use(require('./routes/contact.routes'));
app.use(require('./routes/meting.routes'));
app.use(require('./routes/ticket.routes'));

//Static files
app.use(express.static(path.join(__dirname, 'public')));

module.exports = app;